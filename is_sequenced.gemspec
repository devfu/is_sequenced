Gem::Specification.new do |s|
  s.name        = 'is_sequenced'
  s.version     = '0.0.1'
  s.summary     = 'Add a sequenced attribute to a class.'
  s.description = 'Simplify creation of a sequence column in a class, scoped to another class.'
  s.files       = Dir['lib/**/*.rb']
  s.author      = 'Dev Fu!'
  s.email       = 'info@devfu.com'
  s.homepage    = 'http://github.com/devfu/is_sequenced'
end
