%w[ rubygems spec active_support ].each { |f| require f }
require File.join(File.dirname(__FILE__), '..', 'lib', 'is_sequenced')

class Whatever

  extend IsSequenced

  def save success = true
    return success
  end

end

class Foo < Whatever

  attr_accessor :bar_sequence

  def initialize
    @bar_sequence = 1 # the column should default to 1
  end

end

class Bar < Whatever

  is_sequenced :foo

  attr_accessor :foo, :sequence # these columns/associations should be defined in ActiveRecord

  # fake foo association
  def initialize foo = nil
    @foo = foo || Foo.new
  end

end

describe Foo do

  describe '#increment_bar_sequence!' do

    it 'increments bar_sequence' do
      foo = Foo.new
      foo.bar_sequence.should == 1
      foo.increment_bar_sequence!
      foo.bar_sequence.should == 2
    end

  end

end

describe Bar do

  describe '#set_sequence!' do

    before do
      @foo = Foo.new
      @bar = Bar.new @foo
    end

    it 'gets the sequence from foo' do
      @foo.bar_sequence.should == 1

      @bar.sequence.should be_nil
      @bar.set_sequence!
      @bar.sequence.should == 1
    end

    it 'updates foo#bar_sequence' do
      @foo.bar_sequence.should == 1

      @bar.sequence.should be_nil
      @bar.set_sequence!
      @foo.bar_sequence.should == 2
    end

    it 'increments foo#bar_sequence for each bar instance added' do
      @foo.bar_sequence.should == 1
      @bar.set_sequence!
      @foo.bar_sequence.should == 2

      another_bar = Bar.new @foo
      another_bar.set_sequence!

      @bar.sequence.should        == 1
      another_bar.sequence.should == 2
      @foo.bar_sequence.should    == 3
    end

  end

end
