Is Sequenced
============

Designed for use in ActiveRecord models, with one belonging_to another. Auto-increments sequence columns on both models.

This adds a unique reference column to the dependent model, scoped by the independent model.
The independent model keeps track of what the next number in the sequence should be.
As you add new instances of the dependent model, they will be numbered sequentially.

Notes:

Right now, you're on your own for including this in your models.

 - you're responsible for adding the appropriate columns to your database
 - you're responsible for adding the call to before_create to your model
 - you're responsible for making sure that the sequence columns don't get modified unintentionally
 - you're responsible for making sure that the sequence column is unique

There is no configuration, you're stuck with the column names `CHILD_TABLE_sequence` and `sequence`

Usage:

    class Foo < ActiveRecord::Base

      has_many :bars
      # assumes there is a bars_sequence integer column to the foos_table, with a default value of 1

    end

    class Bar

      belongs_to :foo
      # assumes there is a sequence integer column on the bars_table, without a default value

      is_sequenced :foo
      after_create :set_sequence!

    end

    the_foo = Foo.create
    the_foo.bars_sequence                               # => 1
    a_bar = Bar.create :foo => the_foo
    a_bar.sequence                                      # => 1
    another_bar = Bar.create :foo => the_foo
    another_bar.sequence                                # => 2

    another_foo = Foo.create
    another_foo.bars_sequence                           # => 1
    yet_another_bar = Bar.create :foo => another_foo
    yet_another_bar.sequence                            # => 1

