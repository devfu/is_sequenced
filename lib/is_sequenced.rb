module IsSequenced

  def is_sequenced parent_class_name
    @is_sequenced_parent_name = parent_class_name
    include ChildInstanceMethods

    parent_class = parent_class_name.to_s.classify.constantize
    class_name   = name.underscore

    parent_class.class_eval do
      define_method "increment_#{ class_name }_sequence!" do
        value = send "#{ class_name }_sequence"
        send "#{ class_name }_sequence=", value + 1
        save
      end
    end

  end

  module ChildInstanceMethods

    def is_sequenced_parent_name
      self.class.instance_variable_get '@is_sequenced_parent_name'
    end

    def is_sequenced_parent
      send is_sequenced_parent_name
    end

    def set_sequence!
      class_name    = self.class.name.underscore
      self.sequence = is_sequenced_parent.send "#{class_name}_sequence"
      is_sequenced_parent.send "increment_#{class_name}_sequence!" if save
    end

  end

end
